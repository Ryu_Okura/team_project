//コメント和訳済み
//17t238 追加: BGM
import java.awt.CardLayout;
import java.awt.Dimension;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;

/*
 * The main frame contains all the JPanel used to display the game.
 * In order to show the panels, a CardLayout is used to switch from a view to another.
 * メインフレームには、ゲームの表示に使用されるすべてのJPanelが含まれる
 * パネルを表示するために、"CardLayout"を使用してビューから別のビューに切り替える
 */

@SuppressWarnings("serial")
// java.lang.SuppressWarning 注釈を使用して、
// コンパイル・ユニットのサブセットに関するコンパイル警告を無効化
public class Frame extends JFrame{
	
	private CardLayout cl;
	private JPanel content;
	
	//- 自作クラスの宣言
	private Board board;
	private Menu menu;
	private GameOver gameover;
	private Ranking ranking;
	//-add
	private Bgm bgm = new Bgm(); 
    //
	
	
	public Frame(){
				
		setTitle("ColorGame");
		setSize(new Dimension(800,600));
		setLocationRelativeTo(null);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setResizable(false);
		setFocusable(true);
		
		this.setIconImage((new ImageIcon(getClass().getResource("/logo2.png"))).getImage());
		
		cl = new CardLayout();
		content = new JPanel();
		content.setLayout(cl);
		
		board = new Board();
		menu = new Menu();
		
		content.add(menu, "Menu");
		content.add(board, "Board");
		bgm.play("menu");    // add
		this.add(content);
		
	}
	
	public void newGame(){
		
		content.add(board, "Board");
		cl.show(content, "Board");
		bgm.play("game");    // add
	}
	
	public void gameOver(){
		
		gameover = new GameOver();
		content.add(gameover, "GameOver");
		cl.show(content, "GameOver");
		bgm.play("gameover");    // add
		
	}
	
	public void menu(){
		
		cl.show(content, "Menu");
		bgm.play("menu");    // add
		
		
	}
	
	public void ranking() {
		
		ranking = new Ranking();
		content.add(ranking, "Ranking");
		cl.show(content, "Ranking");
		bgm.play("ranking");    // add
		
	}

}
