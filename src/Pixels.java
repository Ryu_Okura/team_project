//コメント和訳済み

import java.awt.Color;

/*
 * This class manages the colors of the board throughout the game.
 * The colors are stored in a 2D array that represents the board, each case is a square.
 * このクラスは、ゲーム全体を通してボードの色を管理する。
 * 色はボードを表す2次元配列に保存される。また、各要素は正方形である。
 */
public class Pixels {
	
	private Color[][] colors; //the 2D arrays of colors    // ボードにおけるその要素の色を定義
	
	private int height;    // ボードの高さ
	private int width;    // ボードの幅
	private int size; //size of the squares, in pixels  //正方形のサイズ
	private int count; //defines the number of turns left or the number of the player who will play
	// 残りのターン数 or プレイするプレイヤーの数を定義
	
	/*
	 * The size, the number of squares and the count for the current game is given
	 * at the initialization of the game in the Game class.
	 * 現在のゲームのサイズ、正方形の数、countは、Gameクラスのゲームの初期化時に指定される
	 */
	public Pixels(int x, int y, int z, int count){
		
		colors = new Color[x][y];
		
		this.height = x;
		this.width = y;
		this.size = z;
		this.count = count;
		 		
		initColors();
		
	}	
	
	/*
	 * This method is the core of the game.
	 * When the player click on a command button, the top left square
	 * and all the consecutive square of the same color too.
	 * To optimize the game, the method does not check all the cells of the array every time.
	 * From the first square, if a square on the left, top, right or right (←bottom?) is the same color,  
	 * this square get the new color and the method check the neighbors of this square.
	 * "checkAdj",  ゲームの中核メソッド
	 *プレーヤーがコマンドボタンをクリックすると、左上の正方形と同じ色のすべての連続した正方形も同様に操作される..?
	 *	ゲームを最適化するために、このメソッドは毎回配列のすべてのセルをチェックしない。
	 * その代わり、最初の正方形から、左, 上, 右, 下のいずれかの正方形が同じ色の場合、
	 * この正方形は新しい色を取得し、メソッドはこの正方形の四方を連続して(再帰的に)チェックする。
	 */
	public void checkAdj(Color newcolor, Color oldcolor, int i, int j){
		
		//check me!
		if(colors[i][j] == oldcolor) {
			colors[i][j] = newcolor;
		}
		
		//check right square
		if(j+1<width && colors[i][j+1] == oldcolor){ //if this square is the same color of the checked square
			// この正方形がチェックされた正方形と同じ色の場合
			colors[i][j+1] = newcolor; //the color is changed to the new color
			// newcolorで上書きする
			checkAdj(newcolor, oldcolor, i, j+1); //then we check the consecutive square of this changed square
			// 連続する正方形(1つ右)に対し同様のチェックを行う
			
		}
		//- 以下、check right squareとほぼ同様に、左、下、上についてチェックする。
		//check bottom square
		if(i+1<height && colors[i+1][j] == oldcolor){
			colors[i+1][j] = newcolor;
			checkAdj(newcolor, oldcolor, i+1, j);
			
		}
		
		//check left square
		if(j-1>=0 && colors[i][j-1] == oldcolor){
			colors[i][j-1] = newcolor;
			checkAdj(newcolor, oldcolor, i, j-1);
			
		}
		
		//check top square
		if(i-1>=0 && colors[i-1][j] == oldcolor){
			colors[i-1][j] = newcolor;
			checkAdj(newcolor, oldcolor, i-1, j);
		
		}
		// 以上の四方の判定すべてに引っかからなかった場合、そのマスまでで処理は終了する(色が上書きされない)
	}
	
	/*
	 * This function check all the cells of the array and return at the 
	 * moment it meets a different color than the first one.
	 * この関数は、配列のすべてのセルをチェックし、最初のセルとは異なる色に出会った瞬間にfalseを返す
	 */
	public boolean isWinning(){
		
		for(int i=0; i<height; i++){
			for(int j=0; j<width; j++){
				if(colors[i][j] != colors[0][0])
					return false;
			}
		}
		
		return true;
	}
	
	/*
	 * This function is used to change the color of a specific cell.
	 * In the recursive method checkAlign, we don't change the color of the first cell checked.
	 * setColor(): 特定のセルの色を変更するために使用
     * 再帰的なメソッドcheckAlignでは、最初にチェックされたセルの色を変更しない(故にこれを用いる)
	 */
	public void setColor(Color newcolor, int x, int y){
		colors[x][y] = newcolor;
	}
	
	/*
	 * This function initialize the array with random colors.
	 * As there is only 6 colors, there is little risk that the puzzle
	 * will be impossible to finish with the number of turn defined (count).
	 *  initColors(): ランダムな色で配列を初期化する
	 * 6色しかないため、定義されたターン数（カウント）でパズルを終了できないというリスクは
	 * ほとんどない(クリア可能かは保証されていないということ)
	 */
	public void initColors(){
		
		for(int i=0;i<height;i++){
			for(int j=0;j<width;j++){
				int rand = (int)(Math.random()*6);
				switch(rand){
				case 0:
					this.colors[i][j] = Mycolors.blue.getColor();
					break;
				case 1:
					this.colors[i][j] = Mycolors.red.getColor();
					break;
				case 2:
					this.colors[i][j] = Mycolors.green.getColor();
					break;
				case 3:
					this.colors[i][j] = Mycolors.orange.getColor();
					break;
				case 4:
					this.colors[i][j] = Mycolors.yellow.getColor();
					break;
				case 5:
					this.colors[i][j] = Mycolors.purple.getColor();
					break;
				}
			}
		}
	}
	
	
	public Color[][] getColors(){
		return colors;
	}

	public int getHeight(){
		return height;
	}

	public void setHeight(int height){
		this.height = height;
	}

	public int getWidth(){
		return width;
	}

	public void setWidth(int width){
		this.width = width;
	}

	public int getSize(){
		return size;
	}
	
	public void setSize(int size){
		this.size = size;
	}
	
	public int getCount(){
		return count;
	}
	
	public void setCount(int count){
		this.count = count;
	}
	

}
