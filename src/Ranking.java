import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

import javax.swing.JLabel;
import javax.swing.JPanel;

@SuppressWarnings("serial")
public class Ranking extends JPanel {
    public Ranking() {
        super(new GridBagLayout());
        
        this.setBackground(Mycolors.greyblue.getColor());
		
		GridBagConstraints c = new GridBagConstraints();
		c.insets = new Insets(5,5,5,5);	
		
		ArrayList<Integer> scores = new ArrayList<>();
			
		/*
		 * The players can only choose one level of difficulty
		 * so the JRadioButtons are grouper in a Button Group.
		 */
		
		int score = Game.getGame().getPixels().getCount();
		String difficulty = Game.getGame().getdifficulty();
		
		scores = getfile(difficulty);
		scores.add(score);
		
		Collections.sort(scores , Collections.reverseOrder());
		scores.remove(5);
		
		writefile(scores, difficulty);
		
		JLabel jlab = new JLabel("Difficulty :" + difficulty);
		c.gridy = 0;
		c.gridx = 1;
		jlab.setFont(new Font("Tahoma", Font.BOLD, 30));
		this.add(jlab, c);
		
		JLabel winlab = new JLabel("<html><body>"
				   				    + "number1 : " + scores.get(0) + "<br>"
		                            + "number2 : " + scores.get(1) + "<br>"
		                            + "number3 : " + scores.get(2) + "<br>"
		                            + "number4 : " + scores.get(3) + "<br>"
		                            + "number5 : " + scores.get(4) + "<br>"
		                            + "</body></html>");
		
		winlab.setFont(new Font("Tahoma", Font.BOLD, 20));
		
		c.gridy = 1;
		this.add(winlab, c);
		
		
		Button tryagain = new Button(Mycolors.blue.getColor(), "tryagain", "Try Again");
		tryagain.setPreferredSize(new Dimension(150,30));
		c.gridy = 2;
		this.add(tryagain, c);
		
		Button menu = new Button(Mycolors.green.getColor(), "menu", "Menu");
		menu.setPreferredSize(new Dimension(150,30));
		c.gridy = 3;
		this.add(menu, c);
		
	}
	
	/*
	 * This method is used to display the title of the game.
	 * Because the game is based on color change, the color of the title is randomly selected.
	 */
	public void paintComponent(Graphics g){
		
		super.paintComponent(g);
		
		int rand = (int)(Math.random()*6);
		Color color;
		
		switch(rand){
		case 0:
			color = Mycolors.purple.getColor();
			break;
		case 1:
			color = Mycolors.red.getColor();
			break;
		case 2:
			color = Mycolors.green.getColor();
			break;
		case 3:
			color = Mycolors.blue.getColor();
			break;
		case 4:
			color = Mycolors.yellow.getColor();
			break;
		case 5:
			color = Mycolors.orange.getColor();
			break;
		default :
			color = Mycolors.orange.getColor();
			break;
		}
		
		g.setColor(color);
		g.setFont(new Font("Tahoma", Font.BOLD, 60));
		g.drawString("RANKING", 250, 80);
		
	}
	
	
	private static ArrayList<Integer> getfile(String difficulty){  //To Read Ranking file
		ArrayList<Integer> scores = new ArrayList<>();
		File f = new File("scores"+ difficulty +".txt");
		 
		try(Scanner sc = new Scanner(f)){
			
			sc.useDelimiter(",");
			
			while(sc.hasNextLine()){
				for (int i = 0; i < 5; i++) {
					scores.add(i , sc.nextInt());
				}
				
				sc.nextLine();
			}
		} catch (FileNotFoundException e) {
			for (int i = 0; i < 5; i++) {
				scores.add(i , 0);
			}
		}
		return scores;
	}
	
	private static void writefile(ArrayList<Integer> scores, String difficulty) {  // To Write Ranking file
        File f = new File("scores" + difficulty +".txt");
		
		try(BufferedWriter bw = new BufferedWriter(new FileWriter(f))){	
			for (int items = 0; items < 5; items++) {
				String item = scores.get(items).toString();
				bw.write(item);
				bw.write(",");
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
