//コメント和訳済み

import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JLabel;
import javax.swing.JPanel;

/*
 * The JPanel GameOver is displayed at the end of the game.
 * It contains a String that changes according to the result of the game and three buttons to navigate in the game.
 * The components are displayed using a GridBagLayout
 * JPanel GameOverは、ゲームの最後に表示されます。
 * ゲームの結果に応じて変化する文字列と、ゲーム内を移動するための3つのボタンが含まれています。
 * コンポーネントはGridBagLayoutを使用して表示されます
 */
/*
 *新規にランキング画面に移動するためのボタンを追加(O)
 */
@SuppressWarnings("serial")
public class GameOver extends JPanel{
	
	public GameOver(){
		
		super(new GridBagLayout());
		
		this.setBackground(Mycolors.greyblue.getColor());
		
		GridBagConstraints c = new GridBagConstraints();
		c.insets = new Insets(5,5,5,5);
		
		c.gridy = 0;
		
		int score = Game.getGame().getPixels().getCount();
		//--シングルモード時の処理
		JLabel winlab = new JLabel("You win ! Your Score is " + score +"!" ); //in single mode, when the player wins
		//プレイヤー勝利時
		winlab.setFont(new Font("Tahoma", Font.BOLD, 20));
		JLabel looselab = new JLabel("You loose..."); // in single mode, when the player loses
		//プレイヤー敗北時
		looselab.setFont(new Font("Tahoma", Font.PLAIN, 20));
		
		/*
		 * In multiplayer, the count is used to defines which player has to play
		 * Then, at the end of the game, the winner is the last player who played minus one.
		 * マルチプレイヤーでは、カウントはどのプレイヤーがプレイしているかを定義するために使用される
		 *勝者はゲーム終了時にカウントが -1 (= 最後のプレイヤー)である
		 */
		int winner = Game.getGame().getPixels().getCount(); 
		if(winner == 1)
			winner = Game.getGame().getNbofplayers();
		else
			winner--;
		
		JLabel playerwin = new JLabel("Player " + winner + " win !");
		playerwin.setFont(new Font("Tahoma", Font.BOLD, 20));
		
		if(Game.getGame().getPixels().isWinning() && Game.getGame().getNbofplayers() > 1)
			this.add(playerwin, c);
		else if(Game.getGame().getPixels().isWinning())
			this.add(winlab, c);
		else
			this.add(looselab, c);
		
		Button tryagain = new Button(Mycolors.blue.getColor(), "tryagain", "Try Again");
		tryagain.setPreferredSize(new Dimension(150,30));
		c.gridy = 1;
		this.add(tryagain, c);
		
		Button menu = new Button(Mycolors.green.getColor(), "menu", "Menu");
		menu.setPreferredSize(new Dimension(150,30));
		c.gridy = 2;
		this.add(menu, c);
		
		Button exit = new Button(Mycolors.red.getColor(), "exit", "Exit");
		exit.setPreferredSize(new Dimension(150,30));
		c.gridy = 3;
		this.add(exit, c);
		
		//--add O
		Button ranking = new Button(Mycolors.orange.getColor(), "ranking", "Ranking");
		ranking.setPreferredSize(new Dimension(150,30));
		c.gridy = 4;
		this.add(ranking, c);
	}

}
