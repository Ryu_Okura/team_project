//作成者 17t238 坂根早麿
// 他ソースからの流用, 仮実装

public class Bgm {
	
	private Sound sound; //sound object that will contain the different music clip depending of the state of the game
	
	/*
	 * The state parameter is used to define which music has to be played
	 */
	public void play(String state){
		
		if(sound != null)
			sound.stop(); //stop a music before launching another one
		
		if(state == "game"){
			sound = new Sound( "theme2_road.wav");
			sound.loop();
		}
		else if(state == "gameover"){
			sound = new Sound( "theme2_gameover.wav");
		}
		else if(state == "menu"){
			sound = new Sound( "theme2_boss.wav");
		}
		else if(state == "ranking"){
			sound = new Sound("pokemon_road.wav");
		}
		else if(state == "bpush"){
			sound = new Sound("on.wav");
		}
		else {
			sound = new Sound("on.wav");
		}
	}
	
		
}