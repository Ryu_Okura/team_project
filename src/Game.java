// コメント和訳済み

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.AbstractButton;

/*
 * The Game class is the core of the game, it manages the actions for every button,
 * the upgrade of the board, the panel change, etc
 * "Game"クラスはゲームの中核として、全ボタンのアクションを管理する
 * 管理項目: ボードの更新, パネルの変更, etc
 */
public class Game implements ActionListener{

	private static Game game;
	
	private Frame frame;
	private Pixels pixels;
	private Timer t = new Timer(500, new TimerListener());
	
	//-add
	private Bgm se = new Bgm(); 
	//
	
	private int count; //defines the number of turns left or the number of the player who will play
	// 残りのターン数 or プレイするプレイヤーの番号を定義
	private String difficulty;    // 難易度
	private int nbofplayers;     // プレイ人数
	private String name; //name is used to get the name of a button 
	// buttonの名前取得用
	
	public Game(){
		Game.game = this;
		
		frame = new Frame();
		frame.setVisible(true);

		difficulty = "easy"; // default value(easy)
		nbofplayers = 1;    // (1 player)
	}
	
	/*
	 * After every color change, this method is used to verify if the game is finished or not
	 * There is two cases for the game to finish :
	 * either the board is in one color (pixels.isWinning())
	 * or, in single player, the count is equal to 0 (pixels.getCount() <= 0 && nbofplayers == 1)
	 * "checkWin"メソッドは色が変更されるたびに、ゲームが終了したかどうかの判定に使用される
	 *ゲームの終了には以下2パターンが存在
	 *・どちらかのボードが1色(pixels.isWinning())
	 *・シングル時に カウント= 0となるとき (pixels.getCount() <= 0 && nbofplayers == 1)
	 */
	public void checkWin(){  //勝敗判定
		if((pixels.getCount() <= 0 && nbofplayers == 1) || pixels.isWinning()) {
			frame.gameOver();    // gameover処理
		}
	}
	
	/*
	 * The management of the button is divided in two functions : one for the menu button
	 * and one for the command color buttons. 
	 * ボタン管理は2つの機能で行う
	 * メニューボタン,   コマンドカラーボタン
	 * メニュー処理では難易度設定やプレイ人数の設定を行う
	 */
	@Override
	public void actionPerformed(ActionEvent e){
		// AbstractButton: ボタンおよびメニュー項目の共通動作を定義
		String bname = (((AbstractButton)e.getSource()).getName());
		name = bname;
		se.play("bpush");    //add  ボタンが呼び出されるたびにさえずる
		
		if(bname == "blue" || bname == "green" || bname == "red" || bname == "orange" || bname == "purple" || bname == "yellow") {
			t.restart();
            commandbutton();		// 取得bnameがcolorの場合はコマンドカラー処理
		}
		else {
			menubutton();    // それ以外の場合はメニュー処理
		}
        if(name == "oneplayer" || name == "twoplayers" || name == "threeplayers" || name == "fourplayers" || name == "tryagain") {
			t.start();
		}
	}
	//- メニュー処理
	public void menubutton(){
		
		//set the difficulty of the game
		// ゲームの難易度設定
		if(name == "easy" || name == "medium" || name == "hard")
			difficulty = name;
		
		//set the number of players then launch the game with the difficulty previously selected
		// プレイヤーの数を設定しつつ、前項で選択した難易度でゲームを起動
		if(name == "oneplayer"){
			nbofplayers = 1;
			setLevel(difficulty);
		}
		
		if(name == "twoplayers"){
			nbofplayers = 2;
			setLevel(difficulty);
		}
		
		if(name == "threeplayers"){
			nbofplayers = 3;
			setLevel(difficulty);
		}
		
		if(name == "fourplayers"){
			nbofplayers = 4;
			setLevel(difficulty);
		}
		
		//launch a new game, reset the count in single player mode
		// ニューゲームとして再起動しつつ、カウントをリセット(シングルプレイヤーモードにする)
		if(name == "tryagain"){
			if(nbofplayers == 1) {
				pixels.setCount(count);    // countは残りターン数..?
			}
			pixels.initColors();
			frame.newGame();
		}
		
		//in multiplayer, the first player is randomly selected
		// マルチプレイでは、最初のプレイヤーがランダムに選択される
		if(nbofplayers != 1){
			int rand = (int)(Math.random()*nbofplayers)+1;
			pixels.setCount(rand);    // countは操作プレイヤーの指定..?
		}
		
		if(name == "menu"){
			frame.menu();    // メニュー画面の呼び出し
		}
		
		if(name == "exit") {
			//TODO
			System.exit(0);    //frame.dispatchEvent(new WindowEvent(frame, WindowEvent.WINDOW_CLOSING));
		}
		
		//- add O
		if (name == "ranking") {  
			//If button pushed , to go menu
			frame.ranking();
		}
		//
		
	}
	
	//- コマンド処理
	public void commandbutton(){
		
		Color newcolor;
		
		switch(name){
		case "blue":
			newcolor = Mycolors.blue.getColor();
			break;
		case "green":
			newcolor = Mycolors.green.getColor();
			break;
		case "red":
			newcolor = Mycolors.red.getColor();
			break;
		case "orange":
			newcolor = Mycolors.orange.getColor();
			break;
		case "purple":
			newcolor = Mycolors.purple.getColor();
			break;
		case "yellow":
			newcolor = Mycolors.yellow.getColor();
			break;
		default:
			newcolor = Mycolors.blue.getColor();
			break;
		}
		
		Color oldcolor;    //自陣の色: これを変更することで陣地を増やしていく
		
		//in single player mode
		if(nbofplayers == 1){
			//oldcolor is the color of the top-left square
			//シングルモード時、oldcolorの初期値は左上端の色
			oldcolor = pixels.getColors()[0][0];
			if(oldcolor == newcolor) {
				return;
			}
			pixels.checkAdj(newcolor, oldcolor, 0, 0); //change the color of the consecutive squares
			//連続する正方形の色を変更
			pixels.setColor(newcolor, 0, 0); // change the color of the first square
			//最初( (0,0), 左上)の正方形の色を変更
			pixels.setCount(pixels.getCount()-1); //the count is decremented
		}
		
		//in multiplayer mode
		// 基本動作はシングルモードと同様だが、getCount()で現在の操作プレイヤーを参照している
		if(nbofplayers == 2){
			if(pixels.getCount() == 1){
				oldcolor = pixels.getColors()[0][0];
				if(oldcolor == newcolor)
					return;
				pixels.checkAdj(newcolor, oldcolor, 0, 0);
				pixels.setColor(newcolor, 0, 0);
				pixels.setCount(pixels.getCount()+1);
			}
			else{
				//for the second player, the oldcolor is the color of the bottom-right square
				// 2番目のプレイヤーの場合、oldcolorは右下の正方形の色
				oldcolor = pixels.getColors()[pixels.getHeight()-1][pixels.getWidth()-1];
				if(oldcolor == newcolor)
					return;
				pixels.checkAdj(newcolor, oldcolor, pixels.getHeight()-1, pixels.getWidth()-1);
				pixels.setColor(newcolor, pixels.getHeight()-1, pixels.getWidth()-1);
				pixels.setCount(pixels.getCount()-1); //the count is used to show which player turn it is
				// カウントは現在の操作できるプレイヤーを示す
			}
		}
		
		if(nbofplayers == 3){
			if(pixels.getCount() == 1){
				oldcolor = pixels.getColors()[0][0];  
				if(oldcolor == newcolor)
					return;
				pixels.checkAdj(newcolor, oldcolor, 0, 0);
				pixels.setColor(newcolor, 0, 0);
				pixels.setCount(pixels.getCount()+1);
			}
			else if(pixels.getCount() == 2){
				oldcolor = pixels.getColors()[pixels.getHeight()-1][pixels.getWidth()-1];
				if(oldcolor == newcolor)
					return;
				pixels.checkAdj(newcolor, oldcolor, pixels.getHeight()-1, pixels.getWidth()-1);
				pixels.setColor(newcolor, pixels.getHeight()-1, pixels.getWidth()-1);
				pixels.setCount(pixels.getCount()+1);
			}
			else{  // 三人目: 基準は左下端
				oldcolor = pixels.getColors()[pixels.getHeight()-1][0];
				if(oldcolor == newcolor)
					return;
				pixels.checkAdj(newcolor, oldcolor, pixels.getHeight()-1, 0);
				pixels.setColor(newcolor, pixels.getHeight()-1, 0);
				pixels.setCount(1);
			}
		}
		
		if(nbofplayers == 4){
			if(pixels.getCount() == 1){
				oldcolor = pixels.getColors()[0][0];
				if(oldcolor == newcolor)
					return;
				pixels.checkAdj(newcolor, oldcolor, 0, 0);
				pixels.setColor(newcolor, 0, 0);
				pixels.setCount(pixels.getCount()+1);
			}
			else if(pixels.getCount() == 2){
				oldcolor = pixels.getColors()[pixels.getHeight()-1][pixels.getWidth()-1];
				if(oldcolor == newcolor)
					return;
				pixels.checkAdj(newcolor, oldcolor, pixels.getHeight()-1, pixels.getWidth()-1);
				pixels.setColor(newcolor, pixels.getHeight()-1, pixels.getWidth()-1);
				pixels.setCount(pixels.getCount()+1);
			}
			else if(pixels.getCount() == 3){
				oldcolor = pixels.getColors()[pixels.getHeight()-1][0];
				if(oldcolor == newcolor)
					return;
				pixels.checkAdj(newcolor, oldcolor, pixels.getHeight()-1, 0);
				pixels.setColor(newcolor, pixels.getHeight()-1, 0);
				pixels.setCount(pixels.getCount()+1);
			}
			else{  // 四人目: 基準は右上端
				oldcolor = pixels.getColors()[0][pixels.getWidth()-1];
				if(oldcolor == newcolor)
					return;
				pixels.checkAdj(newcolor, oldcolor, 0, pixels.getWidth()-1);
				pixels.setColor(newcolor, 0, pixels.getWidth()-1);
				pixels.setCount(1);
			}
		}
		
		frame.validate();
		frame.repaint();
		
		
		
		checkWin();
		
	}
	
	/*
	 * The three level of difficulty are differentiate by :
	 * the number of squares, their size and the count
	 * 難易度の3レベルは、正方形の数、サイズと数によって区別
	 */
	public void setLevel(String difficulty){
		
		if(difficulty == "easy"){
			pixels = new Pixels(10,6,70,16);
			count = 16;
		}
		else if(difficulty == "medium"){
			pixels = new Pixels(14,9,50,21);
			count = 21;
		}
		else if(difficulty == "hard"){
			pixels = new Pixels(25,16,30,33);
			count = 33;
		}
		
		frame.newGame();
		
	}
//- 以下、値の取得用関数の定義	
	static public Game getGame(){
		return Game.game;
	}
	
	public Pixels getPixels(){
		return pixels;
	}
	
	public int getCount(){
		return count;
	}
	
	public int getNbofplayers(){
		return nbofplayers;
	}
	
	public String getdifficulty() {
		return difficulty;
	}
    
    private class TimerListener implements ActionListener{
		private Color cp;
	    public void actionPerformed(ActionEvent e) {
	    	if(nbofplayers == 1) {
	    		if(pixels.getColors()[0][0].equals(Color.white)) {
	    			pixels.checkAdj(cp, Color.white, 0, 0);
	    		}else {
	    			cp = pixels.getColors()[0][0];
	    			pixels.checkAdj(Color.white, cp, 0, 0);
	    		}
	    	}else {
    	    	if(pixels.getCount() == 1) {
    	    		if(pixels.getColors()[0][0].equals(Color.white)) {
    	    			pixels.checkAdj(cp, Color.white, 0, 0);
    	    		}else {
    	    			cp = pixels.getColors()[0][0];
    	    			pixels.checkAdj(Color.white, cp, 0, 0);
    	    		}
    	    	}else if(pixels.getCount() == 2) {
    	    		if(pixels.getColors()[pixels.getHeight()-1][pixels.getWidth()-1].equals(Color.white)) {
    	    			pixels.checkAdj(cp, Color.white, pixels.getHeight()-1, pixels.getWidth()-1);
    	    		}else {
    	    			cp = pixels.getColors()[pixels.getHeight()-1][pixels.getWidth()-1];
    	    			pixels.checkAdj(Color.white, cp, pixels.getHeight()-1, pixels.getWidth()-1);
    	    		}
    	    	}else if(pixels.getCount() == 3) {
    	    		if(pixels.getColors()[pixels.getHeight()-1][0].equals(Color.white)) {
    	    			pixels.checkAdj(cp, Color.white, pixels.getHeight()-1, 0);
    	    		}else {
    	    			cp = pixels.getColors()[pixels.getHeight()-1][0];
    	    			pixels.checkAdj(Color.white, cp, pixels.getHeight()-1, 0);
    	    		}
    	    	}else {
    	    		if(pixels.getColors()[0][pixels.getWidth()-1].equals(Color.white)) {
    	    			pixels.checkAdj(cp, Color.white, 0, pixels.getWidth()-1);
    	    		}else {
    	    			cp = pixels.getColors()[0][pixels.getWidth()-1];
    	    			pixels.checkAdj(Color.white, cp, 0, pixels.getWidth()-1);
    	    		}
    	    	}
	    	}
			frame.repaint();
		}
	}
}
