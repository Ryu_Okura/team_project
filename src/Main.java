//コメント和訳・追加済み

/*
 * The ColorGame application is a simple game based on a board of colored squares
 * where the goal is to fill the boar with only one color.
 * This program was made from zero and inspired by a mobile game.
 * This game implements new features : the choice of difficulty and a multiplayer mode.
 * ColorGameアプリケーションは、色付きの正方形のボードに基づいたシンプルなゲームです。
 * 目標は、ボードを1色のみで塗りつぶすことです。
 * このプログラムはゼロから作成され、モバイルゲームに触発されました。
 *このゲームには、難易度の選択とマルチプレイヤーモードという新しい機能が実装されています。
 */
/*
 * ColorGameアプリケーションは、色付きの正方形のボードを用いたシンプルなパズルゲームで、
 * ボードを1色のみで塗りつぶすことを目標とします。
 * ESIEE Paris からのインターンンシップ生の作品である"Color game"をもとに改良を加えました。
 *既存の"難易度の選択"と"マルチモード"の機能に加え、
 *"BGM","ヘルプ画面","ランキング機能","自陣の表示の明瞭化"を実装しました。
 */
public class Main {
	
	public static void main(String[] args){
		
		new Game();
		
	}

}
